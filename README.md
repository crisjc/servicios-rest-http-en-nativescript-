
# Servicios REST Http en nativescript 

  
## Palabras claves
#nativescript #rest #angular #servicio #module #http 
## Indice 

1. [Introduccion](#id1) 
2. [Implementacón](#id2) 

  

  

<a name="id1"></a> 

## Introduccion  
Consumir los servicios  **REST** de un backend en nativescript se podria realizar como los utilizados en angular como ***httClient***. Pero nativescript dentro de sus modulos nativos tiene implementado metodos para utilizar servicio  al igual que angular con el ***httClient***.

El módulo ***HTTP*** proporciona una funcionalidad que permite enviar solicitudes GET y POST para ambas plataformas (iOS y Android). Con sus métodos, podríamos solicitar datos de un servidor remoto y representar la respuesta recibida en diferentes formatos. Podríamos solicitar o enviar datos de las siguientes maneras:

* getString
* getJSON
* getImage
* getFile
* request

[link de documentación](https://docs.nativescript.org/ns-framework-modules/http)
<a name="id2"></a> 

## Implementación  

### Imports 
~~~
import { HttpResponse,request, getFile, getImage, getJSON, getString } from "tns-core-modules/http";
~~~
### Metodo **GET**
El ejemplo muestra diferentes formas en que podemos recibir contenido de un servidor al hacer una solicitud HTTP GET.
El ejemplo demuestra, cómo obtener el contenido de request-response y cómo representar los datos recibidos como un valor de cadena u objeto JSON
> service.ts
~~~
    obtenerTodos(){
    return from(request({
        url:'AQUI URL DEL SERVIDOR',
        method:'GET',
    }))
    .pipe(
        map((res:HttpResponse)=>{
            console.log('respuesta en el servicio',res.content.toJSON())
        return res.content.toJSON();
    }))
      }
~~~
  
   ### Metodo **POST**
   
   El ejemplo muestra cómo hacer una solicitud HTTP POST y cómo obtener una respuesta de solicitud.
  ~~~
    crearPiso(usuario:usuarioInteface){
        return from(
            request ({
                url:'AQUI URL DEL SERVIDOR',
                method:'POST',
                content:JSON.stringify(usuario)
            })
        )
        .pipe(map((res:HttpResponse)=>{
            return res.content.toJSON();
        }
            
        ))
    }
~~~

  

  

  

## Redes Sociales 

<a href="https://twitter.com/crisjc8" target="_blank"><img alt="Sígueme en Twitter" height="35" width="35" src="https://3.bp.blogspot.com/-E4jytrbmLbY/XCrI2Xd_hUI/AAAAAAAAIAo/qXt-bJg1UpMZmTjCJymxWEOGXWEQ2mv3ACLcBGAs/s1600/twitter.png" title="Sígueme en Twitter"/> @crisjc8 </a><br> 

<a href="https://linkedin.com/in/cristhian-jumbo-748934180/" target="_blank"><img alt="Sígueme en LinkedIn" height="35" width="35" src="https://4.bp.blogspot.com/-0KtSvK3BydE/XCrIzgI3RqI/AAAAAAAAH_w/n_rr5DS92uk9EWEegcxeqAcSkV36OWEOgCLcBGAs/s1600/linkedin.png" title="Sígueme en LinkedIn"/> Cristhian Jumbo</a><br> 

<a href="https://www.instagram.com/crisjc6/" target="_blank"><img alt="Sígueme en Instagram" height="35" width="35" src="https://4.bp.blogspot.com/-Ilxti1UuUuI/XCrIy6hBAcI/AAAAAAAAH_k/QV5KbuB9p3QB064J08W2v-YRiuslTZnLgCLcBGAs/s1600/instagram.png" title="Sígueme en Instagram"/> crisjc6</a><br> 

 

